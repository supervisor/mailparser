package razborFailaExp;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;

public class RazborMailov {

	public static void main(String[] args) throws IOException {

		String dir = "./in";
		String outDir = "./out";
		String found = null;
		System.out.println(args.length);
		if (args.length > 0) {
			found = args[0];
		}
		String found2 = found;
		List<String> lines = new ArrayList<>();
		Map<String, Set<String>> listByCountry = new HashMap<String, Set<String>>() {
			@Override
			public Set<String> get(Object key) {
				Set<String> set = super.get(key);
				if (set == null) {
					set = new HashSet<String>();
					put((String) key, set);
				}
				return set;
			}
		};
		Map<String, Set<String>> listByDomen = new HashMap<String, Set<String>>() {
			@Override
			public Set<String> get(Object key) {
				Set<String> set = super.get(key);
				if (set == null) {
					set = new HashSet<String>();
					put((String) key, set);
				}
				return set;
			}
		};
		Set<String> linesWithoutPassword = new HashSet<>();

		if (found != null) {
			Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try (Stream<String> stream = Files.lines(file)) {

						List<String> list = new ArrayList<>();
						list = stream.filter(line -> line.contains(found2)).collect(Collectors.toList());
						lines.addAll(list);
					} catch (IOException e) {
						e.printStackTrace();
					}
					return FileVisitResult.CONTINUE;
				}
			});

			FileUtils.writeLines(new File(String.format("./out/%s.txt", found2)), lines);
		}

		if (found == null) {
			Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					try (Stream<String> stream = Files.lines(file)) {
						stream.peek((email) -> {

							try {
								Set<String> tempSet = listByCountry
										.get(email.substring(email.indexOf(".") + 1, email.lastIndexOf(":")));
								tempSet.add(email);

								listByCountry.put(email.substring(email.indexOf(".") + 1, email.lastIndexOf(":")),
										tempSet);
							} catch (Exception e) {
								// TODO: handle exception
							}

						}).peek((email) -> {

							try {
								Set<String> tempSet = listByDomen
										.get(email.substring(email.indexOf("@") + 1, email.lastIndexOf(":")));
								tempSet.add(email);

								listByDomen.put(email.substring(email.indexOf("@") + 1, email.lastIndexOf(":")),
										tempSet);

							} catch (Exception e) {
								// TODO: handle exception
							}
						}).forEach((email) -> linesWithoutPassword.add(email.substring(0, email.lastIndexOf(":"))));

						// lines.addAll(list);
					} catch (Exception e) {
						e.printStackTrace();
					}
					return FileVisitResult.CONTINUE;
				}
			});
		}

		Path directory = Paths.get(outDir);
		try {
			Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}

			});

		} catch (Exception e) {
			// TODO: handle exception
		}
		FileUtils.writeLines(new File("./out/onlyEmails/all.txt"), linesWithoutPassword);

		for (Entry<String, Set<String>> var : listByCountry.entrySet()) {
			FileUtils.writeLines(new File(String.format("./out/byCountry/%s.txt", var.getKey())), var.getValue());
			ArrayList<String> listTemp = (ArrayList<String>) var.getValue().stream()
					.map((email) -> email.substring(0, email.lastIndexOf(":"))).collect(Collectors.toList());
			FileUtils.writeLines(new File(String.format("./out/onlyEmails/byCountry/%s.txt", var.getKey())), listTemp);
		}
		for (Entry<String, Set<String>> var : listByDomen.entrySet()) {
			FileUtils.writeLines(new File(String.format("./out/byDomen/%s.txt", var.getKey())), var.getValue());

			ArrayList<String> listTemp = (ArrayList<String>) var.getValue().stream()
					.map((email) -> email.substring(0, email.lastIndexOf(":"))).collect(Collectors.toList());
			FileUtils.writeLines(new File(String.format("./out/onlyEmails/byDomen/%s.txt", var.getKey())), listTemp);
		}

	}

}
